//
//  ViewController.swift
//  Screen of payments
//
//  Created by Никита Журавлев on 10/11/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var textFieldBackground: UIView!
    
    @IBOutlet var doneButton: UIButton!
    
    
    private func configureUI() {
        textFieldBackground.layer.cornerRadius = 6
        doneButton.layer.cornerRadius = 8
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureUI()
    }


}

